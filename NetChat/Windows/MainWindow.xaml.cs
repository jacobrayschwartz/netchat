﻿using System;
using System.Windows;
using NetChat.NetChatClient;

namespace NetChat.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, MessageReceiver
    {
        public delegate void SendMessageHandler(string message, String ip, string port, EventArgs e);
        public event SendMessageHandler SendMessage;

        public delegate void ClosingApplicationHandler(EventArgs e);

        public event ClosingApplicationHandler ClosingApp;

        public MainWindow()
        {
            InitializeComponent();
        }

        public void receive(Message message)
        {
            string text = message.sender + ":\r\n\t" + message.MessageContent;

            this.Dispatcher.Invoke(() =>
            {
                this.ChatWindowTextField.Text += "\r\n" + text;
            });
        }

        /// <summary>
        /// When the user hits the send button, grab the text, clear the field and fir the SendMesage event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendMesageButton_Click(object sender, RoutedEventArgs e)
        {
            var text = MessageTextField.Text;
            MessageTextField.Text = "";


            SendMessage?.Invoke(text, IpAddressTextField.Text, PortTextField.Text, new EventArgs());
        }



        /// <summary>
        /// When the main window is closed, stop the application
        /// </summary>
        /// <param name="e">Event arguments</param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            ClosingApp?.Invoke(new EventArgs());
            Application.Current.Shutdown();
        }
    }
}
