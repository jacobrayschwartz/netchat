﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NetChat.Windows
{
    /// <summary>
    /// Interaction logic for StartupDialog.xaml
    /// </summary>
    public partial class StartupDialog : Window
    {
        public delegate void StartupDialogFinishedHandler(bool isValid, string name, int port, EventArgs e);

        public event StartupDialogFinishedHandler StartupDialogFinished;

        private string name = null;
        private int port = -1;
        private bool isValid = false;

        public StartupDialog()
        {
            InitializeComponent();
        }

        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.isValid = this.validate();
        }

        private void PortTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.isValid = validate();
        }

        private bool validate()
        {
            string nameText = this.NameTextBox.Text;
            string portText = this.PortTextBox.Text;

            //If the name is empty
            if (null == nameText || "" == nameText.Trim())
            {
                this.BeginButton.IsEnabled = false;
                return false;
            }

            int port;
            bool isPortValid = int.TryParse(portText, out port);

            //Bad port
            if (!isPortValid || port < 0 || port > 65536)
            {
                this.BeginButton.IsEnabled = false;
                return false;
            }

            //Valid, user may being
            this.name = nameText.Trim();
            this.port = port;
            this.BeginButton.IsEnabled = true;
            return true;
        }

        /// <summary>
        /// When the user hits "begin" we are ready to start
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BeginButton_Click(object sender, RoutedEventArgs e)
        {
            StartupDialogFinished?.Invoke(this.isValid, this.name, this.port, new EventArgs());
            this.Close();
        }
    }
}
