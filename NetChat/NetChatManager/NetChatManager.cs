﻿using NetChat.NetChatClient;
using System;

namespace NetChat.NetChatManager
{
    class NetChatManager
    {
        private NetChatListener listener;
        private NetChatSender sender;
        private Windows.MainWindow chatWindow;

        /// <summary>
        /// Manager class for the application, deals with passing off messages, etc.
        /// </summary>
        /// <param name="chatWindow">Main window for the application</param>
        /// <param name="listener">Listener object</param>
        /// <param name="sender">Sender object</param>
        public NetChatManager(Windows.MainWindow chatWindow, NetChatListener listener, NetChatSender sender)
        {
            this.chatWindow = chatWindow;
            this.listener = listener;
            this.sender = sender;

            this.chatWindow.SendMessage += this.SendMessageHandler;
            this.chatWindow.ClosingApp += this.ClosingApplicationHandler;
            this.sender.MessageSent += this.MessageSentHandler;
            this.sender.MessageSendError += this.SendMessageErrorHandler;
            this.listener.MessageReceived += this.ReceiveMessage;
            this.listener.MessageReceiveError += this.ReceiveMessageError;
        }

        /// <summary>
        /// Event handler to deal with the send message event fired from the main window
        /// </summary>
        /// <param name="message">Contents of the message to send</param>
        /// <param name="args">Other event args</param>
        public void SendMessageHandler(string message, string ip, string strPort, EventArgs args)
        {
            int port = -1;
            bool portIsValid = int.TryParse(strPort, out port);

            if (portIsValid)
            {
                sender.SendMessage(message, ip, port);
            }
            else
            {
                chatWindow.receive(new SystemMsg("Port is invalid", DateTime.Now));
            }
        }

        /// <summary>
        /// When a message is successfully sent, display it in the chat history
        /// </summary>
        /// <param name="message"></param>
        /// <param name="e"></param>
        private void MessageSentHandler(NetChatMsg message, EventArgs e)
        {
            SelfMessage selfMessage = new SelfMessage(message.MessageContent, message.TimeStamp);
            chatWindow.receive(selfMessage);
        }

        /// <summary>
        /// Handles when there is an error sending a message
        /// </summary>
        /// <param name="message">Message that was attempted</param>
        /// <param name="error">Error message</param>
        /// <param name="e">Event args</param>
        private void SendMessageErrorHandler(NetChatMsg message, string error, EventArgs e)
        {
            string truncatedText = message.MessageContent;
            if (truncatedText.Length > 15)
            {
                truncatedText = truncatedText.Substring(0, 15) + "...";
            }

            chatWindow.receive(new SystemMsg("Could not send message (" + truncatedText + ") due to error: " + error, DateTime.Now));
        }

        /// <summary>
        /// When a message is received, we report it here
        /// </summary>
        /// <param name="message">Message received</param>
        /// <param name="e">Event args</param>
        private void ReceiveMessage(NetChatMsg message, EventArgs e)
        {
            this.chatWindow.receive(message);
        }

        /// <summary>
        /// Invoked when we received data that could not be demarshalled 
        /// </summary>
        /// <param name="senderIp">IP Address of sender</param>
        /// <param name="senderPort">Port of sender</param>
        /// <param name="error">Error message</param>
        /// <param name="e">Event arguments</param>
        private void ReceiveMessageError(string senderIp, int senderPort, string error, EventArgs e)
        {
            SystemMsg message = new SystemMsg("Could not receive data from " + senderIp + ":" + senderPort + " due to error: " + error, DateTime.Now);
            this.chatWindow.receive(message);
        }

        private void ClosingApplicationHandler(EventArgs e)
        {
            if (this.listener != null)
            {
                this.listener.stopListening();
            }
        }
    }
}
