﻿using System;
using NetChat.NetChatClient;
using System.Windows;
using NetChat.Windows;

namespace NetChat
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public void ApplicationStart(object sender, StartupEventArgs e)
        {
            Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;

            MainWindow window = new Windows.MainWindow();
            this.MainWindow = window;

            StartupDialog startupDialog = new StartupDialog();
            startupDialog.StartupDialogFinished += this.ProcessStartup;
            startupDialog.Show();
        }

        /// <summary>
        /// Handles the event from the startup window
        /// </summary>
        /// <param name="isValid">If parameters are valid, should always be true when event fires correctly</param>
        /// <param name="name">Name of this user</param>
        /// <param name="port">Port to listen on</param>
        /// <param name="e">Event args</param>
        private void ProcessStartup(bool isValid, string name, int port, EventArgs e)
        {
            if (!isValid)
            {
                throw new Exception("Invalid credentials passed to NetChat, please check Name and Port validity.");
            }

            var mainWindow = (MainWindow)this.MainWindow;

            NetChatSender netChatSender = new NetChatSender("127.0.0.1", port, name);
            NetChatListener netChatListener = new NetChatListener(port);

            var manager = new NetChatManager.NetChatManager(mainWindow, netChatListener, netChatSender);

            netChatListener.startListening();

            MainWindow.Show();
            mainWindow.receive(new SystemMsg("Listening on port " + port, DateTime.Now));
        }
    }
}
