﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetChat.NetChatClient
{
    /// <summary>
    /// Generic message class
    /// </summary>
    [Serializable]
    public abstract class Message
    {

        /// <summary>
        /// Returns the message content
        /// </summary>
        public string MessageContent { get;}

        public DateTime TimeStamp { get; }

        /// <summary>
        /// Returns the sender, may be formatted differently depending on the type of message
        /// </summary>
        public abstract string sender { get; }

        protected Message(string message, DateTime timestamp)
        {
            this.MessageContent = message;
            this.TimeStamp = timestamp;
        }

        public override string ToString()
        {
            return "Message: " + MessageContent;
        }
    }

    /// <summary>
    /// General message sent from client to client
    /// </summary>
    [Serializable]
    public class NetChatMsg : Message
    {
        public string SenderIp { get; set; }
        public int SenderPort { get; }
        public string SenderName { get; }
        public string TargetIp { get; }
        public int TargetPort { get; }


        public NetChatMsg(string message, DateTime timestamp, int senderPort, string senderName, string targetIp, int targetPort) : base(message, timestamp)
        {
            this.SenderPort = senderPort;
            this.SenderName = senderName;
            this.TargetIp = targetIp;
            this.TargetPort = targetPort;
            this.SenderIp = "";
        }

        public override string sender
        {
            get
            {
                return SenderName + " [" + SenderIp + ":" + SenderPort + "]";
            }
        }
    }

    /// <summary>
    /// Message to display when user has sent a message, display in chat window as "Me"
    /// </summary>
    public class SelfMessage : Message
    {
        public SelfMessage(string message, DateTime timestamp) : base(message, timestamp)
        {
            //Nothing here...
        }

        public override string sender { get { return "Me"; } }
    }

    /// <summary>
    /// System message, used to inform the user of some issue, eg. send message failed
    /// </summary>
    public class SystemMsg : Message
    {
        public SystemMsg(string message, DateTime timestamp) : base(message, timestamp)
        {
        }

        public override string sender
        {
            get
            {
                return "System";
            }
        }
    }
}
