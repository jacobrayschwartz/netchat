﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetChat.NetChatClient
{
    public interface MessageReceiver
    {
        void receive(Message message);
    }
}
