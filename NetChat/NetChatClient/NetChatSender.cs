﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetChat.NetChatClient
{
    public class NetChatSender
    {
        public int MyPort { get; }
        public string Name { get; }

        public delegate void MessageSentHandler(NetChatMsg message, EventArgs e);

        public delegate void SendMessageErrorHandler(NetChatMsg message, string error, EventArgs e);

        public event MessageSentHandler MessageSent;
        public event SendMessageErrorHandler MessageSendError;

        public NetChatSender(string ip, int port, string name)
        {
            MyPort = port;
            Name = name;
        }

        /// <summary>
        /// Spawns a thread to send the message to the target receiver
        /// If message is sent successfully, MessageSent is fired
        /// If message is not sent due to an error, MessageSendError is fired
        /// </summary>
        /// <param name="content"></param>
        /// <param name="targetIp"></param>
        /// <param name="targetPort"></param>
        public void SendMessage(string content, string targetIp, int targetPort)
        {
            var message = new NetChatMsg(content, DateTime.Now, MyPort, Name, targetIp, targetPort);
            Console.WriteLine("Sending message: " + message);

            IPAddress targetAddress;
            try
            {
                targetAddress = IPAddress.Parse(targetIp);
            }
            catch (Exception e)
            {
                this.MessageSendError?.Invoke(message, e.GetType() + ": " + e.Message, new EventArgs());
                return;
            }

            IPEndPoint endpont = new IPEndPoint(targetAddress, targetPort);

            //Create the socket

            //Fire a thread to connect and send the message
            SendMessage sender = new SendMessage(endpont, message, MessageSent, MessageSendError);
            var thread = new Thread(sender.send);
            thread.Start();
        }
    }

    class SendMessage
    {
        private NetChatMsg message;
        private IPEndPoint endPoint;
        private NetChatSender.MessageSentHandler messageSentEvent;
        private NetChatSender.SendMessageErrorHandler messageErrorEvent;

        public SendMessage(IPEndPoint endPoint, NetChatMsg message, NetChatSender.MessageSentHandler messageSentEvent, NetChatSender.SendMessageErrorHandler messageErrorEvent)
        {
            this.endPoint = endPoint;
            this.message = message;
            this.messageSentEvent = messageSentEvent;
            this.messageErrorEvent = messageErrorEvent;
        }

        public void send()
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                socket.Connect(endPoint);
                MemoryStream stream = new MemoryStream();
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, message);
                socket.Send(stream.ToArray());

                socket.Shutdown(SocketShutdown.Both);
                this.messageSentEvent?.Invoke(this.message, new EventArgs());
            }
            catch (Exception e)
            {
                this.messageErrorEvent?.Invoke(this.message, "Got exception trying to send message " + e.GetType() + ": " + e.Message,
                    new EventArgs());
            }
            finally
            {
                try
                {
                    socket.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Could not close sender socket due to error. " + e.GetType() + ": " + e.Message);
                }
            }
        }
    }
}
