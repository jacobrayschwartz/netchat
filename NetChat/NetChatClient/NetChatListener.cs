﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetChat.NetChatClient
{
    public class NetChatListener
    {
        public delegate void MessageReceivedHandler(NetChatMsg message, EventArgs e);
        public delegate void ReceiveErrorHandler(string senderIp, int senderPort, string error, EventArgs e);

        /// <summary>
        /// Fired when a message is received
        /// </summary>
        public event MessageReceivedHandler MessageReceived;

        /// <summary>
        /// Fired when message demarshal was attempted and failed
        /// </summary>
        public event ReceiveErrorHandler MessageReceiveError;

        public int Port { get; }
        private bool listening = false;

        private Thread listenThread;
        private Socket listener;

        public NetChatListener(int port)
        {
            Port = port;

        }

        public void startListening()
        {
            this.listening = true;
            if (listenThread != null)
            {
                return;
            }

            this.listenThread = new Thread(listen);
            this.listenThread.Start();
        }

        private void listen()
        {
            IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Port);

            this.listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            listener.Bind(localEndPoint);
            listener.Listen(100);

            byte[] bytes = new byte[1024];

            while (this.listening)
            {
                Console.WriteLine("Waiting for data...");

                Socket handler;
                try
                {
                    handler = listener.Accept();
                }
                catch (SocketException e)
                {
                    Console.WriteLine("Error in accepting socket: " + e.GetType() + ": " + e.Message);
                    continue;
                }

                NetChatMsg message = null;

                bytes = new byte[1024];
                int bytesReceived = handler.Receive(bytes);

                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream stream = new MemoryStream(bytes);
                var remoteEP = handler.RemoteEndPoint as IPEndPoint;
                var senderIP = remoteEP.Address.ToString();
                int senderPort = remoteEP.Port;

                try
                {
                    message = (NetChatMsg) formatter.Deserialize(stream);
                    message.SenderIp = senderIP;
                    MessageReceived?.Invoke(message, new EventArgs());
                }
                catch (Exception e)
                {
                    MessageReceiveError?.Invoke(senderIP, senderPort, e.GetType() + ": " + e.Message, new EventArgs());
                }
            }
        }

        public void stopListening()
        {
            this.listening = false;

            if (this.listener != null)
            {
                if (this.listener.Connected)
                {
                    this.listener.Shutdown(SocketShutdown.Both);
                }
                listener.Close();
            }
        }
    }
}
