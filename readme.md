Net Chat
========


A simple chat application I wrote to re-familiarize myself with C#, events/delegates, WPF and Socket programming.


Start the app, enter your name and the port the app will listen on, you may then send a message to any other client, using their address and port. 